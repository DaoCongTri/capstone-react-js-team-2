import { Col, Row } from "antd";
import React from "react";
import { Card } from "antd";
const { Meta } = Card;
function News() {
  return (
    <div className="container pb-5">
      <h1 className="title pt-5 pb-5">CAPS NEWS</h1>
      <Row>
        <Col span={8} className="pb-5">
          <a
            href="https://vtv.vn/van-hoa-giai-tri/blackpink-nghe-si-nu-k-pop-dau-tien-to-chuc-concert-tai-san-van-dong-chau-au-2023071911031862.htm"
            target="blank"
            className="cursor-pointer"
            style={{ textDecoration: "none" }}
          >
            <Card
              className="m-auto border border-solid"
              hoverable
              style={{
                width: 350,
                height: 560,
              }}
              cover={
                <div className="h-[400px]">
                  <img
                    alt="example"
                    className="h-full w-full"
                    src="https://vtv1.mediacdn.vn/thumb_w/650/562122370168008704/2023/7/19/photo1689739278747-1689739278971763470131.jpg"
                  />
                </div>
              }
            >
              <Meta
                title="BLACKPINK - Nghệ sĩ nữ K-Pop đầu tiên tổ chức concert tại sân vận động châu Âu"
                description={
                  <p>
                    VTV.vn - BLACKPINK tiếp tục lập nên kì tích mới khi tổ chức
                    concert tại sân vận động lớn nhất nước Pháp Stade De France.
                  </p>
                }
              />
            </Card>
          </a>
        </Col>
        <Col span={8}>
          <a
            href="https://vietnamnet.vn/rose-blackpink-nhan-sac-thang-hang-tai-san-hon-200-ty-dong-2159835.html"
            target="blank"
            className="cursor-pointer"
            style={{ textDecoration: "none" }}
          >
            <Card
              className="m-auto border border-solid"
              hoverable
              style={{
                width: 350,
                height: 560,
              }}
              cover={
                <div className="h-[400px]">
                  <img
                    alt="example"
                    className="h-full w-full"
                    src="https://static-images.vnncdn.net/files/publish/2023/6/29/001rose-1879.jpg"
                  />
                </div>
              }
            >
              <Meta
                title="Rosé sở hữu ngoại hình xinh đẹp với chiều cao lý tưởng 1,69m, vũ đạo chuyên nghiệp và giọng hát cuốn hút."
                description={
                  <p>
                    Rosé tên thật là Park Chaeyoung, sinh ngày 11/2/1997 tại
                    Auckland, New Zealand. Bố mẹ cô là người Hàn Quốc...
                  </p>
                }
              />
            </Card>
          </a>
        </Col>
        <Col span={8}>
          <a
            href="https://thanhnien.vn/jason-statham-tay-khong-doi-dau-ca-map-bao-chua-trong-meg-2-185230509143711371.htm#"
            target="blank"
            className="cursor-pointer"
            style={{ textDecoration: "none" }}
          >
            <Card
              className="m-auto border border-solid"
              hoverable
              style={{
                width: 350,
                height: 560,
              }}
              cover={
                <div className="h-[400px]">
                  <img
                    alt="example"
                    className="h-full w-full"
                    src="https://images2.thanhnien.vn/thumb_w/640/528068263637045248/2023/5/9/vnvietnammeg2instavertmain1638x2048intl-1683617407262723388676.jpg"
                  />
                </div>
              }
            >
              <Meta
                title="Jason Statham tay không đối đầu cá mập bạo chúa trong 'Meg 2'"
                description={
                  <p>
                    Tựa phim cá mập ăn khách của Hollywood - The Meg sẽ trở lại
                    với phần phim tiếp theo mang tên Meg 2...
                  </p>
                }
              />
            </Card>
          </a>
        </Col>
      </Row>
      <Row>
        <Col span={8}>
          <a
            href="https://tuoitre.vn/park-seo-joon-lo-dien-trong-phim-marvel-hai-huoc-hay-qua-xau-20230412090503453.htm"
            target="blank"
            className="cursor-pointer"
            style={{ textDecoration: "none" }}
          >
            <Card
              className="m-auto border border-solid"
              hoverable
              style={{
                width: 350,
                height: 560,
              }}
              cover={
                <div className="h-[400px]">
                  <img
                    alt="example"
                    className="h-full w-full"
                    src="https://cdn.tuoitre.vn/thumb_w/730/471584752817336320/2023/4/12/park-seo-jooon-1681264165234794010948.jpg"
                  />
                </div>
              }
            >
              <Meta
                title="Park Seo Joon lộ diện trong phim Marvel: Hài hước hay quá xấu?"
                description={
                  <p>
                    Chỉ xuất hiện 1 giây, hình ảnh đầu tiên của Park Seo Joon
                    trong trailer 'The Marvels' gây chú ý. Dân mạng cho rằng tạo
                    hình của anh hài hước, gây cười.
                  </p>
                }
              />
            </Card>
          </a>
        </Col>
        <Col span={8}>
          <a
            href="https://vtv.vn/van-hoa-giai-tri/avatar-the-way-of-water-da-tro-thanh-phim-co-doanh-thu-cao-thu-4-moi-thoi-dai-20230131210353034.htm"
            target="blank"
            className="cursor-pointer"
            style={{ textDecoration: "none" }}
          >
            <Card
              className="m-auto border border-solid"
              hoverable
              style={{
                width: 350,
                height: 560,
              }}
              cover={
                <div className="h-[400px]">
                  <img
                    alt="example"
                    className="h-full w-full"
                    src="https://baoninhbinh.org.vn/DATA/ARTICLES/2023/1/31/avatar-phan-2-lot-top-4-phim-co-doanh-thu-cao-nhat-moi-thoi-3c06a.jpeg"
                  />
                </div>
              }
            >
              <Meta
                title="Avatar: The Way of Water đã trở thành phim có doanh thu cao thứ 4 mọi thời đại"
                description={
                  <p>
                    VTV.vn - Bộ phim dự kiến sẽ còn xếp hạng cao hơn nữa trên
                    bảng xếp hạng doanh thu mọi thời đại này.
                  </p>
                }
              />
            </Card>
          </a>
        </Col>
        <Col span={8}>
          <a
            href="https://baocantho.com.vn/-transformers-7-dan-robot-moi-khoi-dau-moi-a160855.html"
            target="blank"
            className="cursor-pointer"
            style={{ textDecoration: "none" }}
          >
            <Card
              className="m-auto border border-solid"
              hoverable
              style={{
                width: 350,
                height: 560,
              }}
              cover={
                <div className="h-[400px]">
                  <img
                    alt="example"
                    className="h-full w-full"
                    src="https://baocantho.com.vn/image/fckeditor/upload/2023/20230615/images/T13-a1.jpg"
                  />
                </div>
              }
            >
              <Meta
                title="Transformers 7 - Dàn robot mới, khởi đầu mới"
                description={
                  <p>
                    “Transformers: Rise Of The Beasts” (Transformers: Quái thú
                    trỗi dậy) là phần 7 của loạt phim về robot biến hình nổi
                    tiếng...
                  </p>
                }
              />
            </Card>
          </a>
        </Col>
      </Row>
    </div>
  );
}

export default News;
